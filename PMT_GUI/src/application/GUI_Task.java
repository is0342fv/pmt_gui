package application;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class GUI_Task {

	GridPane taskgrid = new GridPane();
	TextArea taskname = new TextArea("");
	TextArea personandplace = new TextArea("");
	List<Label> l_label = new ArrayList<Label>();
	List<Integer> l_status = new ArrayList<Integer>();

	GUI_Task(){
    	// GridPane を taskvbox に１列追加
  	  taskgrid = new GridPane();
  	  taskgrid.setPrefHeight(100);
  	  ColumnConstraints column1 = new ColumnConstraints();
  	  column1.setPercentWidth(33.2);
  	  ColumnConstraints column2 = new ColumnConstraints();
  	  column2.setPercentWidth(33.4);
  	  ColumnConstraints column3 = new ColumnConstraints();
  	  column3.setPercentWidth(33.3);
  	  taskgrid.getColumnConstraints().addAll(column1, column2, column3);

  	  //  各ノード生成
  	  GridPane taskDetails = new GridPane();
  	  GridPane progress = new GridPane();
  	  GridPane comments = new GridPane();

  	  // taskDetails列の設定
  	  ColumnConstraints cctaskname = new ColumnConstraints();
  	  cctaskname.setPercentWidth(70);
  	  ColumnConstraints ccpersonandplace = new ColumnConstraints();
  	  ccpersonandplace.setPercentWidth(30);
  	  taskDetails.getColumnConstraints().addAll(cctaskname, ccpersonandplace);

  	  taskDetails.setPrefHeight(100);
  	  taskDetails.add(taskname, 0, 0);
  	  taskDetails.add(personandplace, 1, 0);

  	  // progress列の設定
  	  List <ColumnConstraints> l_column = new ArrayList<ColumnConstraints>();

  	  int i;
  	  for(i = 0; i<12; i++) {
  		  l_column.add(new ColumnConstraints());
  		  l_column.get(i).setPercentWidth(100);
  		  l_label.add(new Label());
  		  l_status.add(0);

  		  Label t_label = l_label.get(i);
  		  int t_i = i;
  		  t_label.setPrefSize(70, 100);
  		  t_label.setOnMouseClicked( (MouseEvent event) -> {
  			  switch(l_status.get(t_i)) {
  			  case 0:
  	  			  t_label.setBackground(new Background(new BackgroundFill( Color.WHITE , new CornerRadii(0) , Insets.EMPTY )));
  	  			  break;
  			  case 1:
  	  			  t_label.setBackground(new Background(new BackgroundFill( Color.GREEN , new CornerRadii(0) , Insets.EMPTY )));
  	  			  break;
  			  case 2:
  	  			  t_label.setBackground(new Background(new BackgroundFill( Color.RED , new CornerRadii(0) , Insets.EMPTY )));
  	  			  break;
  			  case 3:
  	  			  t_label.setBackground(new Background(new BackgroundFill( Color.YELLOW , new CornerRadii(0) , Insets.EMPTY )));
  	  			  break;
  			  }
  			  int num = l_status.get(t_i);
  			  l_status.set(t_i, (num + 1) % 4);
  		  });
  		  progress.add(t_label, i, 0);
  	  }
  	  progress.setPrefHeight(100);
	  progress.getColumnConstraints().addAll(l_column);
  	  progress.setGridLinesVisible(true);


  	  // comments列の設定


  	  // 各ノードをtaskgridへ追加
  	  taskgrid.add(taskDetails, 0, 0);
  	  taskgrid.add(progress, 1, 0);
  	  taskgrid.add(comments, 2, 0);

  	  taskgrid.setGridLinesVisible(true);

	}

	GridPane getGridPane() {
		return taskgrid;
	}

}
