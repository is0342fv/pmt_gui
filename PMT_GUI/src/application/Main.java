package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application implements Initializable {
    @FXML
    private Label month_3_day_1;

    @FXML
    private Label month_3;

    @FXML
    private Label month_3_day_4;

    @FXML
    private Label month_3_day_2;

    @FXML
    private Label month_2;

    @FXML
    private Label month_3_day_3;

    @FXML
    private Label month_1;

    @FXML
    private TextField filledperson;

    @FXML
    private Label filleddate;

    @FXML
    private GridPane gridpane;

    @FXML
    private Label month_1_day_4;

    @FXML
    private Label managementplan;

    @FXML
    private Label month_1_day_2;

    @FXML
    private Label month_1_day_3;

    @FXML
    private Label month_1_day_1;

    @FXML
    private Label month_2_day_3;

    @FXML
    private Label month_2_day_4;

    @FXML
    private Label month_2_day_1;

    @FXML
    private Label month_2_day_2;

    @FXML
    private ScrollPane taskscrollpane;

    VBox taskvbox;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        // フォント色がおかしくなることへの対処
        System.setProperty( "prism.lcdtext" , "false" );

        // FXMLファイルの読込
        java.net.URL             location    = getClass().getResource( "PMT.fxml" );
        FXMLLoader      fxmlLoader  = new FXMLLoader( location );

        // シーングラフの作成
        Pane    root        = (Pane) fxmlLoader.load();

        // シーンの作成
        Scene scene       = new Scene( root , 1080 , 800 );

        // ウィンドウ表示
        primaryStage.setScene( scene );
        primaryStage.show();

    }

    public static void main(String[] args) {
    	launch(args);
    }

    @FXML
    public void oninsertclicked(ActionEvent event) {
        // 追加ボタンがクリックされた時の動作

    	GUI_Task gui_task = new GUI_Task();
    	taskvbox.getChildren().addAll(gui_task.getGridPane());


    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO 自動生成されたメソッド・スタブ

		taskvbox = new VBox();
		taskvbox.prefWidthProperty().bind(gridpane.widthProperty());;

		taskscrollpane.setContent(taskvbox);

	}
}
